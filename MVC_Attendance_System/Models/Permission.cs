﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC_Attendance_System.Models
{public enum Status { UnKnown, Approved, Rejected}
    public class Permission
    {
        [ForeignKey("Student")]
        [Key, Column(Order = 0)]     
        public string StudentID { get; set; }
        [Key, Column(Order = 1)]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public string Note { get; set; }
        [DefaultValue("UnKnown")]
        public Status status { get; set; }
        public ApplicationUser Student { get; set; }
    }
}