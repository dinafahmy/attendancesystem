﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Attendance_System.Models
{
    public class Department
    {
        public int? DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public List<ApplicationUser> Students { get; set; }


    }
}