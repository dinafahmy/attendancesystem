﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC_Attendance_System.Models
{
    public class Attendance
    {
        [ForeignKey("Student")]
        [Key, Column(Order =0)]
        public string StudentID { get; set; }
        [Key, Column(Order = 1)]
        [Display(Name = "Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{YYYY-MM-DD}")]
        public DateTime Date { get; set; }
        public DateTime Time_Attendance { get; set; }
        public DateTime? Time_Leave { get; set; }
       
        public ApplicationUser Student { set; get; }

    }
}