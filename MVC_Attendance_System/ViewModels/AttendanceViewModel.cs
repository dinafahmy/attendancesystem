﻿using MVC_Attendance_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Attendance_System.ViewModels
{
    public class AttendanceViewModel
    {
        public ApplicationUser Student { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time_Attendance { get; set; }
        public DateTime Time_Leave { get; set; }

    }
}