﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC_Attendance_System.ViewModels
{
    public class StudentDateSummary
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public int OnTime { get; set; }
        public int Late { get; set; }
        public int Absent { get; set; }
        public int Permissions { get; set; }

    }
}