namespace MVC_Attendance_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "DepartmentID", "dbo.Departments");
            DropIndex("dbo.AspNetUsers", new[] { "DepartmentID" });
            AlterColumn("dbo.AspNetUsers", "DepartmentID", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "DepartmentID");
            AddForeignKey("dbo.AspNetUsers", "DepartmentID", "dbo.Departments", "DepartmentID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "DepartmentID", "dbo.Departments");
            DropIndex("dbo.AspNetUsers", new[] { "DepartmentID" });
            AlterColumn("dbo.AspNetUsers", "DepartmentID", c => c.Int(nullable: false));
            CreateIndex("dbo.AspNetUsers", "DepartmentID");
            AddForeignKey("dbo.AspNetUsers", "DepartmentID", "dbo.Departments", "DepartmentID", cascadeDelete: true);
        }
    }
}
