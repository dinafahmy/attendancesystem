namespace MVC_Attendance_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_Attendance_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attendances",
                c => new
                    {
                        StudentID = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Time_Attendance = c.DateTime(nullable: false),
                        Time_Leave = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudentID, t.Date })
                .ForeignKey("dbo.AspNetUsers", t => t.StudentID, cascadeDelete: true)
                .Index(t => t.StudentID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Attendances", "StudentID", "dbo.AspNetUsers");
            DropIndex("dbo.Attendances", new[] { "StudentID" });
            DropTable("dbo.Attendances");
        }
    }
}
