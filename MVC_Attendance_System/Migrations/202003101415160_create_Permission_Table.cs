namespace MVC_Attendance_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_Permission_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        StudentID = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Note = c.String(),
                        status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudentID, t.Date })
                .ForeignKey("dbo.AspNetUsers", t => t.StudentID, cascadeDelete: true)
                .Index(t => t.StudentID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Permissions", "StudentID", "dbo.AspNetUsers");
            DropIndex("dbo.Permissions", new[] { "StudentID" });
            DropTable("dbo.Permissions");
        }
    }
}
