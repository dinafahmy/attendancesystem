namespace MVC_Attendance_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class departmentId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "department_DepartmentID", "dbo.Departments");
            DropIndex("dbo.AspNetUsers", new[] { "department_DepartmentID" });
            RenameColumn(table: "dbo.AspNetUsers", name: "department_DepartmentID", newName: "DepartmentID");
            AlterColumn("dbo.AspNetUsers", "DepartmentID", c => c.Int(nullable: true));
            CreateIndex("dbo.AspNetUsers", "DepartmentID");
            AddForeignKey("dbo.AspNetUsers", "DepartmentID", "dbo.Departments", "DepartmentID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "DepartmentID", "dbo.Departments");
            DropIndex("dbo.AspNetUsers", new[] { "DepartmentID" });
            AlterColumn("dbo.AspNetUsers", "DepartmentID", c => c.Int());
            RenameColumn(table: "dbo.AspNetUsers", name: "DepartmentID", newName: "department_DepartmentID");
            CreateIndex("dbo.AspNetUsers", "department_DepartmentID");
            AddForeignKey("dbo.AspNetUsers", "department_DepartmentID", "dbo.Departments", "DepartmentID");
        }
    }
}
