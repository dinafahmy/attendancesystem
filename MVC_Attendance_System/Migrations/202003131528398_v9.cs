namespace MVC_Attendance_System.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v9 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Attendances", "Time_Leave", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Attendances", "Time_Leave", c => c.DateTime(nullable: false));
        }
    }
}
