﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC_Attendance_System.Models;
using MVC_Attendance_System.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC_Attendance_System.Controllers
{
    public class StudentController : Controller
    {
        UserManager<ApplicationUser> userManager;
        ApplicationDbContext db = new ApplicationDbContext();
        public StudentController()//constructor
        { userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
        }
        // GET: Student
        public async Task<ActionResult> Index()
        {
            //.Users htgeb el users ely 3al system kolo w traga3 IQueryable h7wlha le list (ApplicationUser) w ha-loop 3ala el users ely feha 
            List<ApplicationUser> users = userManager.Users.ToList<ApplicationUser>();
           
            List<ApplicationUser> students = new List<ApplicationUser>(); 
            foreach (var item in users)
            {
                if (await userManager.IsInRoleAsync(item.Id, "Student") == true)
                {
                    students.Add(item); 
                }
            }
            List<Department> departments = db.Departments.ToList<Department>();
            ViewBag.departments = new SelectList(departments, "DepartmentId", "DepartmentName");
            //h3ml 2 select list for std & courses (list name hyloop 3aleha, value, text/name)
            ViewBag.Students = new SelectList(students, "Id", "UserName");
            

            return View();
        }
        //Security View
        [Authorize(Roles = ("Security"))]
        public ActionResult GetStudentsInDepartment(int? id,int? choice)
        {
          List<SelectListItem>  listbox_name=  new List<SelectListItem>()
    {
    new SelectListItem { Text = "Attendance", Value = "1" },
    new SelectListItem { Text = "Leave", Value = "2" }
    };
            List<Department> departments = db.Departments.ToList<Department>();
            ViewBag.list = new SelectList(listbox_name,  "Value", "Text", choice);
            ViewBag.departments = new SelectList(departments, "DepartmentID", "DepartmentName",id);
            return View();
        }
        [Authorize(Roles = ("Security"))]
        public async Task<ActionResult> showStudentsInDept(int id)
        {
            List<ApplicationUser> students = new List<ApplicationUser>();
            List<Attendance> attendances = db.Attendances.ToList<Attendance>();

            List<ApplicationUser> users = userManager.Users.ToList<ApplicationUser>();
            foreach (ApplicationUser item in users)
            {
                if (await userManager.IsInRoleAsync(item.Id, "Student") == true && item.DepartmentID == id && 
                    !attendances.Exists(a=>
                {
                    bool flag=true;
                      if(a.StudentID != item.Id) { flag = false; }
                    else if (a.StudentID == item.Id && a.Date != DateTime.Today) { flag = false; }
                  
                    return flag;
                }))
                    { 
                    students.Add(item); 
                }
            }
            ViewBag.students = new SelectList(students, "Id", "UserName");

            return PartialView(students);          
        }
        [Authorize(Roles = ("Security"))]
        public ActionResult Attend(string std_id,int dept_id)
        {
            db.Attendances.Add(new Attendance() { StudentID = std_id, Date = DateTime.Today, Time_Attendance = DateTime.Now });
            db.SaveChanges();
            return RedirectToAction("GetStudentsInDepartment",new { id = dept_id ,choice=1});
        }
        [Authorize(Roles = ("Security"))]
        public async Task<ActionResult> showLeaveStudent(int id)
{
    List<ApplicationUser> students = new List<ApplicationUser>(); 
    List<Attendance> attendances = db.Attendances.ToList<Attendance>();

    List<ApplicationUser> users = userManager.Users.ToList<ApplicationUser>();
    foreach (ApplicationUser item in users)
    {
        if (await userManager.IsInRoleAsync(item.Id, "Student") == true && item.DepartmentID == id && attendances.Exists(a => ((a.StudentID == item.Id&&a.Date==DateTime.Today) && (a.Time_Leave==null))))//if role of user is students
        {
            students.Add(item); 
        }
    }
        ViewBag.students = new SelectList(students, "Id", "UserName");

        return PartialView("showStudentsInDept", students);
   
}
        [Authorize(Roles = ("Security"))]
        public ActionResult Leave (string std_id, int dept_id)
        {
           var std= db.Attendances.FirstOrDefault(A => A.StudentID == std_id&&A.Date==DateTime.Today);
            std.Time_Leave = DateTime.Now;
            db.SaveChanges();
            return RedirectToAction("GetStudentsInDepartment", new { id = dept_id, choice = 2 });
        }
        //first Admin view
        [Authorize(Roles = ("Admin"))]
        public ActionResult AttendanceInOneDate(int? dept_ID)
        {
            List<Department> departments = db.Departments.ToList<Department>();
            ViewBag.departments = new SelectList(departments, "DepartmentID", "DepartmentName", dept_ID);       
            return View();
        }
        [Authorize(Roles = ("Admin"))]
        public async Task<ActionResult> ShowAttendanceInOneDate(int id,string attend)
        {
            List<Attendance> attendance = db.Attendances.ToList<Attendance>();           
            List<ApplicationUser> students = new List<ApplicationUser>();
            List<ApplicationUser> users = userManager.Users.ToList<ApplicationUser>();
            List<Permission> permission = db.permissions.ToList<Permission>();
            foreach (ApplicationUser item in users)
            {              
                if (await userManager.IsInRoleAsync(item.Id, "Student") == true && item.DepartmentID == id)
                 {
                    students.Add(item);
                 }               
            }
            ViewBag.date = attend;
             ViewBag.students = students;
             ViewBag.permissions = permission;
            return PartialView(attendance);
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult ShowStdBetweenDates()
        {
            List<Department> departments = db.Departments.ToList<Department>();
            ViewBag.department = new SelectList(departments, "DepartmentId", "DepartmentName");
            ViewBag.attend = db.Attendances;
            return View();
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult StudentDaysSummary(int? id, string fromDate, string toDate)
        {
            DateTime FDate = DateTime.Parse(fromDate);
            DateTime TDate = DateTime.Parse(toDate);
            int days = (int)(TDate - FDate).TotalDays;
            DateTime f = FDate.Date;
            DateTime t = TDate.Date;
            //string query = "SELECT s.Id,s.UserName, " +
            //"COUNT(distinct CASE WHEN CAST(a.Time_Attendance AS time) <= " +
            //"  '09:00:00' THEN a.StudentID END) AS 'OnTime', " +
            //"COUNT(distinct CASE WHEN CAST(a.Time_Attendance AS time) > " +
            //"  '09:00:00' THEN a.StudentID END) AS 'Late', " +
            //"(" + days + " - COUNT(distinct CASE WHEN CAST(a.Time_Attendance AS time) <= " +
            //"  '09:00:00' THEN a.StudentID END) - " +
            //"    COUNT(distinct CASE WHEN CAST(a.Time_Attendance AS time) > " +
            //"    '09:00:00' THEN a.StudentID END))AS 'Absent' " +
            //"FROM " +
            //"Attendances a " +
            //"  INNER JOIN AspNetUsers s " +
            //"    ON a.StudentID = s.id " +
            //"  INNER JOIN Departments d " +
            //"    ON d.DepartmentID = " + id + " and s.DepartmentID = d.DepartmentID " +
            //"WHERE " +
            //"  a.Date BETWEEN '" + f + "' AND '" + t + "' " +
            //" GROUP BY s.Id,s.UserName";


            string query = "SELECT s.Id,s.UserName," +
            " COUNT(distinct CASE WHEN CAST(a.Time_Attendance AS time) <= " +
              " '09:00:00' THEN a.StudentID END) AS 'OnTime',  " +
             "COUNT(distinct CASE WHEN CAST(a.Time_Attendance AS time) > " +
               "'09:00:00' THEN a.StudentID END) AS 'Late', " +
            " ("+days+" - COUNT(distinct CASE WHEN CAST(a.Time_Attendance AS time) <=  " +
               "'09:00:00' THEN a.StudentID END) - COUNT( case when s.Id = p.StudentID then p.Date end)- " +
                " COUNT(distinct CASE WHEN CAST(a.Time_Attendance AS time) > " +
                 "'09:00:00' THEN a.StudentID END))AS 'Absent', " +
            " COUNT( case when s.Id = p.StudentID and p.status=1  then p.Date end) AS Permissions " +
                "FROM Attendances a  "  +
             "right JOIN AspNetUsers s  " +
                   " on a.StudentID = s.id   " +
                 "left join Permissions p  " +
                 "on p.StudentID = s.id "+
            
                           "  WHERE (a.Date BETWEEN ' " + f + " ' AND  '" + t + " ' " +
                             " or p.Date BETWEEN '" + f+ "' AND '" + t + "')and s.DepartmentID =  "+ id +
                            " GROUP BY s.Id,s.UserName  ";

            //string qurey2 = "select p.StudentID,COUNT(distinct p.Date) AS Permissions" +
            //                "from Permissions p" +
            //               "  a.Date BETWEEN '" + f + "' AND '" + t + "' " +
            //               " GROUP BY StudentID ";
            //IEnumerable<StudentDateSummary> data2 = db.Database.SqlQuery<StudentDateSummary>(qurey2);

            IEnumerable<StudentDateSummary> data = db.Database.SqlQuery<StudentDateSummary>(query);
            return PartialView(data.ToList());
        }


    }
}