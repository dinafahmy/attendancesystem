﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MVC_Attendance_System.Models;
using MVC_Attendance_System.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC_Attendance_System.Controllers
{
    public class PremissionController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        UserManager<ApplicationUser> userManager;
        public PremissionController()
        {
            userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
        }
        // GET: Premission
        [Authorize(Roles = ("Admin"))]
        public ActionResult AdminPremission(string date)
        {           
            if (date == null)
            {
               date= DateTime.Now.ToString("dd/MM/yyyy");
            }
            ViewBag.date = date;
            return View();
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult _ShowPremissions(string attend)
        {
            List<Permission> SendPremissions = new List<Permission>();
            List<Permission> _Premissions = new List<Permission>();
            _Premissions = db.permissions.ToList();
            SendPremissions = _Premissions.Where(a=>a.Date.ToString("dd/MM/yyyy").Substring(0, 10).Equals(attend)&&a.status==0).ToList();
            ViewBag.premissions = SendPremissions;
            ViewBag.students= userManager.Users.ToList<ApplicationUser>();
            return PartialView("_ShowPremissions");
        }
        [Authorize(Roles = ("Admin"))]
        public ActionResult Approved(string date, string std_id)
        {
            List<Permission> perl = db.permissions.ToList();
          
            var per = perl.FirstOrDefault(m =>(( m.StudentID == std_id) && (m.Date.ToString("dd/MM/yyyy") == date)) );
            per.status = Status.Approved;
            db.SaveChanges();
            return RedirectToAction("AdminPremission", new { date = date });
        }
     
    }
}